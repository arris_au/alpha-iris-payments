@foreach ($methods as $method)
    <div class="payment_method_heading" id="{{ $method->getKey() }}_heading">
        <input
            id="{{ $method->getKey() }}_input"
            type="radio"
            class="payment-radio-selector"
            name="payment_method"
            value="{{ $method->getKey() }}"
            {{ old('payment_method') == $method->getKey() ? 'checked' : '' }}
            onChange="togglePayment(this)"
        >
        <label for="{{ $method->getKey() }}_input" class="font-normal">
            {{ $method->getName() }}
        </label>
    </div>
    <div class="payment_method_body" id="{{ $method->getKey() }}_body" style="display: none">
        @if ($errors->has($method->getKey()))
            @foreach ($errors->get($method->getKey()) as $error)
                <span class="field-error">{{ $error }}</span>
            @endforeach
        @endif

        {!! $method->getFields($request, $order->grand_total, $order->id) !!}
    </div>
@endforeach

@push('footer-scripts')
    <script>
        function togglePayment(item) {
            $('input[name="payment_method"]').each(function(idx, itm) {
                $('#' + itm.value + '_body').hide();
            });

            if (item.checked) {
                $('#' + item.value + '_body').show();
            } else {
                $('#' + item.value + '_body').hide();
            }
        }

        $(document).ready(function() {
            $('input[name="payment_method"][checked]').each(function(idx, itm) {
                togglePayment(itm);
            });
        });
    </script>
@endpush
