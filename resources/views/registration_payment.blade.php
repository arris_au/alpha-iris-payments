@extends('voyager-pages::layouts.default')
@section('meta_title', setting('site.title') . ' - Register')
@section('page_title', 'Register')

@section('content')
    <div class="alpha-iris-margin-element py-10">
        {!! Form::open(['route' => ['register.process', $user]]) !!}

        <input type="hidden" name="order_id" value="{{ $order->id }}" />

        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif


        <div>Membership Type: {{ $mType->name }}</div>
        <div>Joining Fee: {{ $mType->display_join_cost }}</div>
        <div>Ongoing Fee: {{ $mType->display_ongoing_cost }}</div>

        <div>
            @if ($errors->has('payment_method'))
                @foreach ($errors->get('payment_method') as $error)
                    <div class="field-error">{{ $error }}</div>
                @endforeach
            @endif

            @include('alpha-iris-payments::payment_selector')
        </div>
        <div>
            <button type="submit" class="btn btn-default">Register</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
