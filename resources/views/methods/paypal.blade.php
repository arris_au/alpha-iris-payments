<style>
    #paypal-button-container iframe.component-frame {
        z-index: 0 !important;
    }
</style>

<script src="https://www.paypal.com/sdk/js?client-id={{ $clientId }}&intent=authorize&currency={{ $currency }}">
    // Required. Replace YOUR_CLIENT_ID with your sandbox client ID.
</script>

<div id="paypal-button-container"></div>
<input type="hidden" name="paypal_order_id" id="paypal_order_id" value="" />

<script>
    paypal.Buttons({
        createOrder: function(data, actions) {
            // This function sets up the details of the transaction, including the amount and line item details.
            const event = new Event("paypal-create-order");
            document.dispatchEvent(event);
            return actions.order.create({
                purchase_units: [{
                    custom_id: '{{ $orderId }}',
                    amount: {
                        value: '{{ $amount }}'
                    }
                }]
            });
        },
        onApprove: function(data, actions) {
            // This function captures the funds from the transaction.
            return actions.order.authorize().then(function(authorization) {
                // This function shows a transaction success message to your buyer.
                const element = document.getElementById('paypal-button-container');
                element.innerHTML = '';
                element.innerHTML = '<h3>Thank you for your payment!</h3>';

                const input = document.getElementById('paypal_order_id');
                input.value = authorization.purchase_units[0].payments.authorizations[0].id;
                input.form.submit();
            });
        },
        onError: function(err) {
            console.log(err);
        }
    }).render('#paypal-button-container');
</script>
