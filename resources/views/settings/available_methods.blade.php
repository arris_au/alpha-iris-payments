@php
    $methods = \AlphaIris\Payments\Services\Payments::allMethods();
@endphp

@foreach ($methods as $method)
    <div class="form-group">
        @php
            $checked = $method->available();
        @endphp
        <input
            id="{{ $method->getKey() }}"
            type="checkbox"
            name="{{ $method->getKey() }}"
            onChange="aiTogglePaymentMethod('{{ $method->getKey() }}', this.checked)"
            class="toggleswitch"
            @if ($checked) checked @endif
            data-on="On"
            data-off="Off"
        >
        <label for="{{ $method->getKey() }}" class="ml-3">{{ $method->getName() }}</label>
    </div>
@endforeach
<input
    type="hidden"
    name="{{ $setting->key }}"
    id="{{ $setting->key }}"
    value="{{ $setting->value }}"
/>
<script>
    function aiTogglePaymentMethod(method, value) {
        try {
            currentValue = JSON.parse($('#payment\\.payment_methods').val());
        } catch (e) {
            currentValue = {};
        }
        currentValue[method] = value;

        $('#payment\\.payment_methods').val(JSON.stringify(currentValue));
    }
</script>
