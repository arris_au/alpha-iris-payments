<?php

use AlphaIris\Core\Models\Country;
use AlphaIris\Core\Models\State;
use Illuminate\Database\Migrations\Migration;
use TCG\Voyager\Models\Setting;

class AddDefaultSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $setting = Setting::firstOrNew(['key' => 'payment.store_country']);
        $setting->fill([
            'display_name' => 'Store Country',
            'type' => 'table_select',
            'order' => 7,
            'details' => json_encode(
                [
                    'model' => Country::class,
                    'index_name' => 'id',
                    'display_name' => 'name',
                ]
            ),
            'group' => 'Payment',
        ])->save();

        $setting = Setting::firstOrNew(['key' => 'payment.store_state']);
        $setting->fill([
            'display_name' => 'Store State',
            'type' => 'table_select',
            'order' => 7,
            'details' => json_encode(
                [
                    'model' => State::class,
                    'index_name' => 'id',
                    'display_name' => 'name',
                ]
            ),
            'group' => 'Payment',
        ])->save();

        $setting = Setting::firstOrNew(['key' => 'payment.payment_methods']);
        $setting->fill([
            'display_name' => 'Available Methods',
            'type' => 'view',
            'order' => 3,
            'details' => 'alpha-iris-payments::settings.available_methods',
            'value' => '{}',
            'group' => 'Payment',
        ])->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            $setting = Setting::firstOrFail(['key' => 'payment.store_country']);
            $setting->delete();
        } catch (Exception $e) {
        }
        try {
            $setting = Setting::firstOrNew(['key' => 'payment.store_state']);
            $setting->delete();
        } catch (Exception $e) {
        }
    }
}
