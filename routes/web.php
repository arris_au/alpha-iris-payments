<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('register/payment/{user}', '\AlphaIris\Payments\Http\Controllers\PaidRegistrationController@register')->name('register.payment');
    Route::post('register/payment/{user}', '\AlphaIris\Payments\Http\Controllers\PaidRegistrationController@store')->name('register.process');
});
Route::post('paypal/notify', '\AlphaIris\Payments\Http\Controllers\PaypalNotification')->name('paypal.notify');
