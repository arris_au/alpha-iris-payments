<?php

namespace AlphaIris\Payments\Http\Controllers;

use AlphaIris\Core\Http\Controllers\AlphaIrisController;
use AlphaIris\Core\Models\User;
use AlphaIris\Core\Models\UserMembership;
use AlphaIris\Core\Services\PublicFieldsService;
use AlphaIris\Payments\Services\Payments;
use AlphaIris\Payments\Services\PaymentsService;
use AlphaIris\Shopping\Models\Order;
use AlphaIris\Shopping\Models\OrderItem;
use AlphaIris\Shopping\Models\OrderStatus;
use Illuminate\Http\Request;
use TCG\Voyager\Models\DataType;
use AlphaIris\Shopping\Events\OrderPaidEvent;

class PaidRegistrationController extends AlphaIrisController
{
    public function register(User $user, Request $request)
    {
        if (! (optional(auth()->user())->id == $user->id || optional(auth()->user())->hasPermission('update_members'))) {
            if (! auth()->user()) {
                session()->put('login-redirect', route('register.payment', $user));

                return response()->redirectTo(route('login'));
            } else {
                abort(404);
            }
        }

        $mType = $user->membership->membership_type;
        $user->membership->status = UserMembership::MEMBERSHIP_PENDING;
        $user->save();

        $oI = OrderItem::where('model_class', get_class($user->membership))
            ->whereHas('order', function ($q) {
                $q->where('payment_status', '!=', PaymentsService::PAYMENT_CLEARED);
            })
            ->where('model_id', $user->membership->id)
            ->latest()
            ->first();

        if ($oI) {
            $order = $oI->order;

            // check if membership has passed grace period
            if ($this->is_new_member($user)) {
                $oI->delete();
                $order->addItem($user->membership, $mType->name, $mType->cost + $mType->join_cost, 1);
                $order->save();
            }

        } else {
            $pending = OrderStatus::where('name', 'Pending')->firstOrFail();
            $order = Order::create([
                'user_id' => optional(auth()->user())->id,
                'order_status_id' => $pending->id,
                'first_name' => $user->name,
                'last_name' => $user->lastname,
                'email' => $user->email,
                'phone' => $user->phone ?: ' ',
                'address_1' => $user->address_1,
                'address_2' => $user->address_2,
                'suburb' => $user->suburb,
                'state_id' => $user->state_id,
                'postcode' => $user->postcode,
                'country_id' => $user->country_id,
                'shipping_first_name' => $user->name,
                'shipping_last_name' => $user->lastname,
                'shipping_address_1' => $user->address_1,
                'shipping_address_2' => $user->address_2,
                'shipping_suburb' => $user->suburb,
                'shipping_state_id' => $user->state_id,
                'shipping_postcode' => $user->postcode,
                'shipping_country_id' => $user->country_id,
            ]);

            // check if user is new or exceed max days of payment
            if ($this->is_new_member($user)) {
                $order->addItem($user->membership, $mType->name, $mType->cost + $mType->join_cost, 1);
            } else {
                $order->addItem($user->membership, $mType->name, $mType->cost, 1);
            }

            $order->save();
        }

        // check if it's a new member again in case the member created an order but didn't pay the first time
        $is_new_member = $this->is_new_member($user);

        return view('alpha-iris-payments::registration_payment', [
            'request' => $request,
            'order' => $order,
            'orderId' => $order->id,
            'prevRequest' => $request->all(),
            'cost' => $mType->cost,
            'user' => $user,
            'join_cost' => $mType->join_cost,
            'mType' => $mType,
            'amount' => $order->grand_total,
            'methods' => Payments::availableMethods(),
            'is_new_member' => $is_new_member,
        ]);
    }

    public function is_new_member(User $user)
    {
        // check memberships with CLEARED payment status
        $memberships = $user->memberships->where('payment_status', PaymentsService::PAYMENT_CLEARED);

        // No CLEARED payment means it is a new member
        if ($memberships->isEmpty()) {
            return true;
        }

        // check last membership expired date
        $expires_date = $memberships->sortByDesc('expires_at')->first()->expires_at;
        $max_tolerance_days = setting('membership.renew_grace_period');

        if ($expires_date->addDays($max_tolerance_days)->isPast()) {
            return true;
        }

        return false;
    }

    public function store(User $user, Request $request)
    {
        $oI = OrderItem::where('model_class', get_class($user->membership))
            ->where('model_id', $user->membership->id)
            ->latest()
            ->first();

        if ($oI) {
            $order = $oI->order;
        } else {
            throw new \Exception('No pending membership payment found');
        }

        $methods = Payments::availableMethods();
        $request->validate(['payment_method' => 'required']);
        $name = $request->get('payment_method');
        $method = $methods->filter(function ($model) use ($name) {
            return $model->getKey() == $name;
        })->first();

        $method->validate($request);
        $payment_status = $method->process($request, $order->grand_total, $order->id);

        $membership = $user->memberships()->whereIn('status', [
            UserMembership::MEMBERSHIP_PROCESSING,
            UserMembership::MEMBERSHIP_PENDING,
        ])
        ->latest()
        ->first();

        if (is_object($payment_status)) {
            $membership->payment_status = PaymentsService::PAYMENT_PROCESSING;
            $membership->status = UserMembership::MEMBERSHIP_PROCESSING;
            $membership->save();

            return $payment_status;
        } else {
            $membership->payment_status = $payment_status;

            if ($payment_status == PaymentsService::PAYMENT_CLEARED) {
                $membership->status = UserMembership::MEMBERSHIP_ACTIVE;
                $membership->paid_at = now();
            } else {
                $membership->status = UserMembership::MEMBERSHIP_PROCESSING;
            }

            $membership->save();

            // update order status too
            if ($payment_status == PaymentsService::PAYMENT_CLEARED) {
                $order->payment_status = PaymentsService::PAYMENT_CLEARED;
                if (setting('shopping.order_paid_status')) {
                    $order->order_status_id = setting('shopping.order_paid_status');
                }
                $order->save();
                OrderPaidEvent::dispatch($order);
            }
        }

        return response()->redirectTo(route('user.profile'));
    }

    protected function getPublicRegistrationFields()
    {
        $userDT = DataType::where('name', 'users')->first();
        $allFields = PublicFieldsService::fields($userDT);

        return $allFields;
    }
}
