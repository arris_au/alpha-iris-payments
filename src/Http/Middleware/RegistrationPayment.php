<?php

namespace AlphaIris\Payments\Http\Middleware;

use AlphaIris\Core\Models\MembershipType;
use AlphaIris\Payments\Http\Controllers\PaidRegistrationController;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;

class RegistrationPayment
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  callable  $next
     * @return \Illuminate\Http\Response
     */
    public function handle(Request $request, $next)
    {
        if ($request->route()->getName() == 'register.post' || $request->route()->getName() == 'user.process_create_membership') {
            $mType = MembershipType::find($request->get('membership_type'));

            if ($mType && ($mType->cost > 0 || $mType->join_cost > 0)) {
                $result = $next($request);
                if ($result->exception) {
                    return $result;
                }
                $user = auth()->user();

                return response()->redirectTo(route('register.payment', $user));
            } else {
                return $next($request);
            }
        } else {
            return $next($request);
        }
    }
}
