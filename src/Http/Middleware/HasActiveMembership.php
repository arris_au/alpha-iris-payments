<?php

namespace AlphaIris\Payments\Http\Middleware;

use AlphaIris\Core\Models\MembershipType;
use AlphaIris\Core\Models\UserMembership;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;

class HasActiveMembership
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  callable  $next
     * @return \Illuminate\Http\Response
     */
    public function handle(Request $request, $next, ...$guards)
    {
        $route = $request->route();

        $allowedRoutes = ['user.process_create_membership'];
        if(!in_array($route->getName(), $allowedRoutes)){
            if (in_array('auth', $route->computedMiddleware)) {
                $user = auth()->user();
                if ($user && ! $user->hasPermission('update_members')) {
                    $membership = $user->membership;
                    if (! $membership) {
                        throw new AuthenticationException(
                            'Unauthenticated.', $guards, $this->redirectTo($request, $user)
                        );
                    }

                    if ($membership->status !== UserMembership::MEMBERSHIP_ACTIVE) {
                        throw new AuthenticationException(
                            'Unauthenticated.', $guards, $this->redirectTo($request, $user, $membership)
                        );
                    }
                }
            }
        }

        return $next($request);
    }

    protected function redirectTo($request, $user, $membership = null)
    {
        if (! $membership) {
            return route('user.create_membership');
        } else {
            switch ($membership->status) {
                case UserMembership::MEMBERSHIP_EXPIRED:
                    return route('user.expired');
                case UserMembership::MEMBERSHIP_PROCESSING:
                    return route('user.processing');
                default:
                case UserMembership::MEMBERSHIP_PENDING:
                    $request->session()->flash('message', 'Complete your payment to activate your membership');
                    $request->session()->flash('message-class', 'alert-error');

                    return route('register.payment', $user);
            }
        }
    }
}
