<?php

namespace AlphaIris\Payments\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public $fillable = [
        'code',
        'name',
    ];
}
