<?php

namespace AlphaIris\Payments\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $fillable = [
        'amount',
        'method_class',
        'order_reference',
        'external_reference',
        'additional_data',
    ];
}
