<?php

namespace AlphaIris\Payments\Interfaces;

use Illuminate\Http\Request;

interface PaymentServiceInterface
{
    public function process(Request $request, $amount, $orderReference);

    public function available();

    public function validate(Request $request);

    public function getName();

    public function getDescription();

    public function getFields(Request $request, $amount, $orderId);

    public function getKey();

    public function boot();
}
