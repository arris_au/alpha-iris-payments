<?php

namespace AlphaIris\Payments\Providers;

use AlphaIris\Core\Models\UserMembership;
use AlphaIris\Payments\Console\Commands\CreatePaymentMethod;
use AlphaIris\Payments\Http\Middleware\HasActiveMembership;
use AlphaIris\Payments\Http\Middleware\RegistrationPayment;
use AlphaIris\Payments\Methods\BankDeposit;
use AlphaIris\Payments\Methods\PayPal;
use AlphaIris\Payments\Models\Currency;
use AlphaIris\Payments\Services\Payments;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Models\Setting;

class PaymentsServiceProvider extends EventServiceProvider
{
    public function boot()
    {
        try {
            $setting = Setting::firstOrNew(['key' => 'site.currency']);
            $setting->fill([
                'display_name' => 'Currency',
                'type' => 'table_select',
                'order' => 7,
                'details' => json_encode(
                    [
                        'model' => Currency::class,
                        'index_name' => 'code',
                        'display_name' => 'name',
                    ]
                ),
                'group' => 'Site',
            ])->save();
        } catch (\Exception $e) {
        }
        Payments::add(new BankDeposit());
        Payments::add(new PayPal());

        $router = $this->app->make(Router::class);
        $router->pushMiddlewareToGroup('web', RegistrationPayment::class);
        $router->pushMiddlewareToGroup('web', HasActiveMembership::class);
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        $this->loadRoutesFrom(__DIR__.'../../../routes/web.php');
    }

    public function register()
    {
        $this->commands([
            CreatePaymentMethod::class,
        ]);
        $this->loadViewsFrom([
            App::basePath().'/resources/views/alpha-iris-payments',
            __DIR__.'/../../resources/views/',
            __DIR__.'/../../resources/views/vendor/alpha-iris',
        ], 'alpha-iris-payments');
    }
}
