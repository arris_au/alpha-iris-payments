<?php

namespace AlphaIris\Payments\Methods;

use AlphaIris\Payments\Models\Transaction;
use AlphaIris\Payments\Services\PaymentsService;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\PayPal as ServicesPayPal;
use TCG\Voyager\Models\Setting;

class PayPal extends AbstractPaymentMethod
{
    public function process(Request $request, $amount, $orderReference)
    {
        $provider = new ServicesPayPal();
        $provider->setCurrency(setting('site.currency'));
        $provider->setApiCredentials(static::getProviderConfig());
        $token = $provider->getAccessToken();

        $order = $provider->captureAuthorizedPayment($request->get('paypal_order_id'), $orderReference, $amount, 'Complete');

        if (! isset($order['status'])) {
            throw new \Exception('Invalid Order ID');
        }

        if ($order['status'] !== 'COMPLETED') {
            throw new \Exception('Invalid Order ID');
        }

        Transaction::create([
            'amount' => $amount,
            'method_class' => static::class,
            'order_reference' => $orderReference,
            'external_reference' => $order['id'],
            'additional_data' => json_encode($order),
        ]);

        return PaymentsService::PAYMENT_CLEARED;
    }

    public static function getWebhookId()
    {
        return setting('paypal.live_mode') ? setting('paypal.live_webhook') : setting('paypal.sandbox_webhook');
    }

    public static function getClientId()
    {
        return setting('paypal.live_mode') ? setting('paypal.live_client_id') : setting('paypal.sandbox_client_id');
    }

    public static function getProviderConfig()
    {
        $config = [
            'mode' => setting('paypal.live_mode') ? 'live' : 'sandbox',
            'sandbox' => [
                'client_id' => setting('paypal.sandbox_client_id'),
                'client_secret' => setting('paypal.sandbox_secret'),
                'app_id' => setting('paypal.sandbox_account_id'),
            ],
            'live' => [
                'client_id' => setting('paypal.live_client_id'),
                'client_secret' => setting('paypal.live_secret'),
                'app_id' => setting('paypal.live_account_id'),
            ],
            'payment_action' => 'Sale',
            'currency' => setting('site.currency'),
            'notify_url' => route('paypal.notify'),
            'locale' => 'en_US',
            'validate_ssl' => true,
        ];

        return $config;
    }

    public function validate(Request $request)
    {
        $request->merge([
            $this->getKey() => 'method',
        ]);

        if ($request->get('paypal_order_id') == '') {
            $request->validate([
                $this->getKey() => function ($attribute, $value, $fail) {
                    $fail('Please use the PayPal checkout buttons to complete your payment');
                },
            ]);
        }

        $provider = new ServicesPayPal();
        $provider->setCurrency(setting('site.currency'));
        $provider->setApiCredentials(static::getProviderConfig());
        $token = $provider->getAccessToken();

        $order = $provider->showAuthorizedPaymentDetails($request->get('paypal_order_id'));
        $error = null;

        if (! isset($order['status'])) {
            $error = 'Invalid Order ID';
        }

        if ($error) {
            $request->validate([
                $this->getKey() => function ($attribute, $value, $fail) use ($error) {
                    $fail($error);
                },
            ]);
        }

        return true;
    }

    public function getName()
    {
        return 'PayPal';
    }

    public function getDescription()
    {
    }

    public function getFields(Request $request, $amount, $orderId)
    {
        return view('alpha-iris-payments::methods.paypal', [
            'clientId' => static::getClientId(),
            'amount' => $amount,
            'orderId' => $orderId,
            'currency' => setting('site.currency'),

        ])->render();
    }

    public function boot()
    {
        Setting::firstOrCreate(
            ['key' => 'paypal.live_mode'],
            [
                'display_name' => 'Live Mode',
                'value' => false,
                'type' => 'checkbox',
                'order' => 1,
                'group' => 'PayPal',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'paypal.live_account_id'],
            [
                'display_name' => 'Account Id',
                'type' => 'text',
                'order' => 2,
                'group' => 'PayPal',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'paypal.live_client_id'],
            [
                'display_name' => 'Client Id',
                'type' => 'text',
                'order' => 3,
                'group' => 'PayPal',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'paypal.live_secret'],
            [
                'display_name' => 'Secret',
                'type' => 'text',
                'order' => 4,
                'group' => 'PayPal',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'paypal.live_webhook'],
            [
                'display_name' => 'Webhook ID',
                'type' => 'text',
                'order' => 5,
                'group' => 'PayPal',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'paypal.sandbox_account_id'],
            [
                'display_name' => 'Sandbox Account Id',
                'type' => 'text',
                'order' => 6,
                'group' => 'PayPal',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'paypal.sandbox_client_id'],
            [
                'display_name' => 'Sandbox Client Id',
                'type' => 'text',
                'order' => 7,
                'group' => 'PayPal',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'paypal.sandbox_secret'],
            [
                'display_name' => 'Sandbox Secret',
                'type' => 'text',
                'order' => 8,
                'group' => 'PayPal',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'paypal.sandbox_webhook'],
            [
                'display_name' => 'Sandbox Webhook ID',
                'type' => 'text',
                'order' => 9,
                'group' => 'PayPal',
            ]
        );
    }
}
