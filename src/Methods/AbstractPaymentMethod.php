<?php

namespace AlphaIris\Payments\Methods;

use AlphaIris\Payments\Interfaces\PaymentServiceInterface;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;

abstract class AbstractPaymentMethod implements PaymentServiceInterface
{
    public $key;

    public function boot()
    {
    }

    public function __construct()
    {
        $this->key = $this->getKey();
    }

    public function getKey()
    {
        return str_replace('\\', '_', static::class);
    }

    public function available()
    {
        $methods = json_decode(setting('payment.payment_methods'), true);
        if (isset($methods[$this->getKey()])) {
            return $methods[$this->getKey()];
        } else {
            return false;
        }
    }
}
