<?php

namespace AlphaIris\Payments\Methods;

use AlphaIris\Payments\Models\Transaction;
use AlphaIris\Payments\Services\PaymentsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Models\Setting;

class BankDeposit extends AbstractPaymentMethod
{
    public function process(Request $request, $amount, $orderReference)
    {
        Transaction::create([
            'amount' => $amount,
            'method_class' => static::class,
            'order_reference' => $orderReference,
            'external_reference' => $request->get('paypal_order_id'),
        ]);

        return PaymentsService::PAYMENT_PROCESSING;
    }

    public function validate(Request $request)
    {
        return true;
    }

    public function getName()
    {
        return 'Bank Deposit';
    }

    public function getDescription()
    {
    }

    public function getFields(Request $request, $amount, $orderId)
    {
        return view('alpha-iris-payments::methods.bank_deposit', [
            'bsb' => setting('bank-deposit.bsb'),
            'acct_name' => setting('bank-deposit.account_name'),
            'acct_no' => setting('bank-deposit.account_number'),
        ])->render();
    }

    public function boot()
    {
        Setting::firstOrCreate(
            ['key' => 'bank-deposit.bsb'],
            [
                'display_name' => 'BSB',
                'type' => 'text',
                'order' => 1,
                'group' => 'Bank Deposit',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'bank-deposit.account_number'],
            [
                'display_name' => 'Account Number',
                'type' => 'text',
                'order' => 2,
                'group' => 'Bank Deposit',
            ]
        );

        Setting::firstOrCreate(
            ['key' => 'bank-deposit.account_name'],
            [
                'display_name' => 'Account Name',
                'type' => 'text',
                'order' => 3,
                'group' => 'Bank Deposit',
            ]
        );
    }
}
