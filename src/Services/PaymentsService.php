<?php

namespace AlphaIris\Payments\Services;

use AlphaIris\Payments\Interfaces\PaymentServiceInterface;

class PaymentsService
{
    protected $_services = null;
    const PAYMENT_CLEARED = 1;
    const PAYMENT_PROCESSING = 2;
    const PAYMENT_DECLINED = 3;
    const PAYMENT_CANCELLED = 4;
    const PAYMENT_REFUNDED = 5;

    public function __construct()
    {
        $this->_services = collect();
    }

    /**
     * Get possible payment statuses.
     *
     * @return array
     */
    public function statuses()
    {
        return [
            static::PAYMENT_PROCESSING => 'Processing',
            static::PAYMENT_CLEARED => 'Cleared',
            static::PAYMENT_CANCELLED => 'Cancelled',
            static::PAYMENT_DECLINED => 'Declined',
            static::PAYMENT_REFUNDED => 'Refunded',
        ];
    }

    /**
     * Add a payment service.
     *
     * @param PaymentServiceInterface $paymentService
     * @return void
     */
    public function add(PaymentServiceInterface $paymentService)
    {
        $this->_services->add($paymentService);

        $request = app()->request;
        if (substr($request->getPathInfo(), 0, 7) == '/admin/') {
            $paymentService->boot();
        }
    }

    /**
     * Undocumented function.
     *
     * @return void
     */
    public function availableMethods()
    {
        $avail = collect();
        foreach ($this->_services as $service) {
            if ($service->available()) {
                $avail->add($service);
            }
        }

        return $avail;
    }

    public function allMethods()
    {
        return clone $this->_services;
    }

    public function formatCurrency($amount)
    {
        return '$'.number_format($amount, 2);
    }

    public function priceIncTax($price)
    {
        return round($price + config('cart_manager.tax_percentage') * $price / 100, 2);
    }
}
