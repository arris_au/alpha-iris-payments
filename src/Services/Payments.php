<?php

namespace AlphaIris\Payments\Services;

use AlphaIris\Payments\Interfaces\PaymentServiceInterface;
use Illuminate\Support\Facades\Facade;

/**
 * @method static void add(PaymentServiceInterface $paymentService)
 * @method static \Illuminate\Support\Collection availableMethods()
 * @method static \Illuminate\Support\Collection allMethods()
 * @method static array statuses()
 * @method static string formatCurrency($amount)
 *
 * @see \AlphaIris\Core\PublicFieldsServiceManager
 */
class Payments extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return PaymentsService::class;
    }
}
