<?php

use AlphaIris\Payments\Services\PaymentsService;

if (! function_exists('payments')) {
    /**
     * Returns an instance of the Cart class.
     */
    function payments()
    {
        return app(PaymentsService::class);
    }
}
