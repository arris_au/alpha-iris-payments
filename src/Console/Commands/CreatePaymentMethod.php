<?php

namespace AlphaIris\Payments\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class CreatePaymentMethod extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:payment-method';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a new payment method';

    protected $type = 'PaymentMethod';

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\PaymentMethods';
    }

    protected function getStub()
    {
        $filePath = __DIR__;
        $filePath .= '/../Stubs/PaymentMethod.stub';

        return $filePath;
    }
}
